package org.hw1;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        double first;
        double second;
        double rez;
        char action;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your first number: ");
        first = sc.nextDouble();
        System.out.println("Enter your second number: ");
        second = sc.nextDouble();
        System.out.println("Enter your action (+, - , /, *, % )");
        action = sc.next().charAt(0);
        switch (action){
            case '+': rez = first + second;
            break;
            case '-': rez = first - second;
            break;
            case '/': rez = first / second;
            break;
            case '*': rez = first * second;
            break;
            case '%': rez = first % second;
            break;
            default:
                System.out.println("You enter incorrect action");
                return;
        }
        System.out.println("Result: " + rez);



    }


}
