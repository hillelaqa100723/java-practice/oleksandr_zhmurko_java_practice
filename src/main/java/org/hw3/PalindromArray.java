package org.hw3;

public class PalindromArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 3, 2, 1};
        boolean pal = true;

        for (int i = 0; i < array.length / 2; i++) {
            if (array[i] != array[array.length - i - 1]) {
                pal = false;
                break;
            }
        }
        if (pal) {
            System.out.println("Is polindrome");
        } else {
            System.out.println("Isn't palindrome");
        }
    }


}

