package org.hw8;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class EnumFileCalculator {
    enum Opearation {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        REM("%");
        private final String op;

        Opearation(String op) {
            this.op = op;
        }

        public String getOp() {
            return op;
        }
    }


    public static void main(String[] args) throws IOException {

        List<String> dataFromFile = getDataFromFile(new File("files/file.csv"));
        System.out.println(prepareResultString(dataFromFile, calculate(dataFromFile)));
    }

    public static Double calculate(List<String> data) {
        if (data.isEmpty()) {
            System.out.println("Щось пішло не так");
            return null;
        }


        double result = 0;
        Opearation opearation = Opearation.valueOf(data.get(1).toUpperCase());
        double fNumber = Double.parseDouble(data.get(0));
        double sNumber = Double.parseDouble(data.get(2));

        switch (opearation) {
            case PLUS:
                result = fNumber + sNumber;
                break;
            case MINUS:
                result = fNumber - sNumber;
                break;
            case MULTIPLY:
                result = fNumber * sNumber;
                break;
            case DIVIDE:
                if (sNumber != 0) {
                    result = fNumber / sNumber;
                } else {
                    System.out.println("Не можна ділити на нуль");
                    return null;
                }
                break;
            case REM:
                if (sNumber != 0) {
                    result = fNumber % sNumber;
                } else {
                    System.out.println("Не можна ділити на нуль");
                }

        }
        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        String resultStr;
        Opearation opearation = Opearation.valueOf(data.get(1).toUpperCase());
        String operand = opearation.getOp();
        double fNumber = Double.parseDouble(data.get(0));
        double sNumber = Double.parseDouble(data.get(2));
        switch (opearation) {
            case PLUS:
                resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                break;
            case MINUS:
                resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                break;
            case MULTIPLY:
                resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                break;
            case DIVIDE:
                if (sNumber != 0) {
                    resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                } else {
                    return null;
                }
            case REM:
                if (sNumber != 0) {
                    resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                } else {
                    return null;
                }
                break;
            default:
                return null;
        }
        return resultStr;
    }
    public static List<String> getDataFromFile(File dataFile) throws IOException {
        List<String> list = FileUtils.readLines(dataFile, Charset.defaultCharset());
        List<String> resultList = new ArrayList<>();
        for (String str:list.subList(0, list.size())){
            String [] splString = str.split(",");
            resultList.add(0, splString[0].trim());
            resultList.add(1, splString[1].trim());
            resultList.add(2, splString[2].trim());
        }return resultList;
    }

}
