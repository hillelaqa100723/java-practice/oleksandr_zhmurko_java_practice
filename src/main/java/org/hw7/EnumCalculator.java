package org.hw7;

import java.util.ArrayList;
import java.util.List;

import static org.hw7.EnumCalculator.Opearation.MINUS;
import static org.hw7.EnumCalculator.Opearation.PLUS;

public class EnumCalculator {


    enum Opearation {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        REM("%");
        private final String op;

        Opearation(String op) {
            this.op = op;
        }

        public String getOp() {
            return op;
        }
    }


    public static void main(String[] args) {
        List<String> list = List.of("5", "Plus", "5");
        System.out.println(prepareResultString(list, calculate(list)));
    }

    public static Double calculate(List<String> data) {
        if (data.isEmpty()) {
            System.out.println("Щось пішло не так");
            return null;
        }


        double result = 0;
        Opearation opearation = Opearation.valueOf(data.get(1).toUpperCase());
        double fNumber = Double.parseDouble(data.get(0));
        double sNumber = Double.parseDouble(data.get(2));

        switch (opearation) {
            case PLUS:
                result = fNumber + sNumber;
                break;
            case MINUS:
                result = fNumber - sNumber;
                break;
            case MULTIPLY:
                result = fNumber * sNumber;
                break;
            case DIVIDE:
                if (sNumber != 0) {
                    result = fNumber / sNumber;
                } else {
                    System.out.println("Не можна ділити на нуль");
                    return null;
                }
                break;
            case REM:
                if (sNumber != 0) {
                    result = fNumber % sNumber;
                } else {
                    System.out.println("Не можна ділити на нуль");
                }

        }
        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        String resultStr;
        Opearation opearation = Opearation.valueOf(data.get(1).toUpperCase());
        String operand = opearation.getOp();
        double fNumber = Double.parseDouble(data.get(0));
        double sNumber = Double.parseDouble(data.get(2));
        switch (opearation) {
            case PLUS:
                resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                break;
            case MINUS:
                resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                break;
            case MULTIPLY:
                resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                break;
            case DIVIDE:
                if (sNumber != 0) {
                    resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                } else {
                    return null;
                }
            case REM:
                if (sNumber != 0) {
                    resultStr = fNumber + " " + operand + " " + sNumber + " = " + result;
                } else {
                    return null;
                }
                break;
            default:
                return null;
        }
        return resultStr;
    }
}
