package org.hw5;

public class SumArray {
    public static void main(String[] args) {
        int [][] array  = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        sumsInRows(array);
    }
    public static int [] sumsInRows(int [][] array1){
        int [] countArray = new int[array1.length];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                countArray[i] += array1[i][j];
            }
        }
        for (int sum:countArray){
            System.out.println(sum);
        }
        return countArray;
    }
}
