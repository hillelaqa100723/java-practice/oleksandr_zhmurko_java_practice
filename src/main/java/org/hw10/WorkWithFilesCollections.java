package org.hw10;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class WorkWithFilesCollections {
    public static void main(String[] args) throws IOException {
        File file = new File("files/data.csv");
        Map<Integer, User> dataFromFile = getDataFromFile(file);
        System.out.println(dataFromFile);

        User dataById = getDataById(dataFromFile, 8562);
        System.out.println(dataById);

        int name = getNumberOfOccurrences(dataFromFile, "Ivanov");
        System.out.println(name);

        List<User> usersAgeMoreThen = getUsersAgeMoreThen(dataFromFile, 38);
        for (User user:usersAgeMoreThen){
            System.out.println(user.getFirstName() + " " + user.getLastName() + " " + user.getAge());
        }

        System.out.println();

        List<User> employee = new ArrayList<>(dataFromFile.values());
        Map<String, List<Integer>> fullNames = findFullNames(employee);
        for (Map.Entry<String, List<Integer>> entry : fullNames.entrySet()){
            System.out.println("FullName: " + entry.getKey());
            System.out.println("ID: " + entry.getValue());
        }



    }

    public static Map<Integer, User> getDataFromFile(File dataFile) throws IOException {
        Map<Integer, User> students = new HashMap<>();
        List<String> result = FileUtils.readLines(dataFile, Charset.defaultCharset());
        for (String str : result) {
            var index = Integer.parseInt(str.split(",")[0]);
            User user = new User(str);
            students.put(index, user);

        }
        return students;


    }

    public static User getDataById(Map<Integer, User> mapData, Integer id){
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, User> mapData, String lastName){
        int count = 0;
        for (User lst:mapData.values()){
            if (lst.lastName.equalsIgnoreCase(lastName)){
                count++;
            }
        }return count;
    }

    public static List<User> getUsersAgeMoreThen(Map<Integer, User> mapData, int age){
        List<User> ageUsers = new ArrayList<>();
        for (User ageUser:mapData.values()){
            if (ageUser.age > age){
                ageUsers.add(ageUser);
            }
        }return ageUsers;

    }

    public static Map<String, List<Integer>> findFullNames(List<User> users) {
        Map<String, List<Integer>> result = new HashMap<>();

        for (User user : users) {
            String fullName = user.getFirstName() + " " + user.getLastName();

            if (result.containsKey(fullName)) {
                result.get(fullName).add(user.getId());
            } else {
                List<Integer> idList = new ArrayList<>();
                idList.add(user.getId());
                result.put(fullName, idList);
            }
        }

        return result;
    }

    static class User {
        private int id;
        private String firstName;
        private String lastName;
        private int age;

        public User() {

        }

        public User(int id, String firstName, String lastName, int age) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }

        public User(String str) {
            String[] split = str.split(",");

            id = Integer.parseInt(split[0].trim());
            firstName = split[2].trim();
            lastName = split[1].trim();
            age = Integer.parseInt(split[3].trim());

        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof User user)) return false;
            return age == user.age && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(firstName, lastName, age);
        }

        @Override
        public String toString() {
            return "User{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}

