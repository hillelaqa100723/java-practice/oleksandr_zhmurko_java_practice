package org.hw9;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class WorkWithFiles {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\s_zhm\\oleksandr_zhmurko_java_practice\\files\\data.txt");
        Map<Integer, String> users = new HashMap<>();
        users = getDataFromFile(file);
        System.out.println(getDataById(users, 2586));
        System.out.println(getNumberOfOccurrences(users,"petrov"));


    }
    public static Map<Integer, String> getDataFromFile(File dataFile) throws IOException {
        Map<Integer,String> user = new HashMap<>();
        List<String> strings = FileUtils.readLines(dataFile, Charset.defaultCharset());
        for (String str:strings){
            String[] split = str.split(",");
            user.put(Integer.valueOf(split[0]), split[1]);
        }
        return user;

        }
    public static String getDataById(Map<Integer, String> mapData, Integer id){
        return mapData.get(id);
    }
    public static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName){
        int cnt = 0;
        List <String> names = new ArrayList<>(mapData.values());
        for (String name:names){
            if (name.split(" ")[0].equalsIgnoreCase(lastName)){
                cnt++;
            }
        }return cnt;
    }

    }


